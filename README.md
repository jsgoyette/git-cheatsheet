Intro
=====

Git only tracks changes to files, and does not store the actual file.

`man git-commit` and other flags have a wealth of information.

It’s good to keep stable versions while developing. When developing a
feature, keep a separate branch.

GUI’s gitk is a nice graphical representation.

Starting New Projects
=====================

`git init` to create a new `.git` directory.

`git remote add origin url` to set the remote pointer.

`git clone url` to clone a pre-existing project to your
current directory.

`git pull origin master` to pull the current remote; even better is `git pull --rebase origin master` 
so that the changes on master are integrated without having to do superfluous merge commit.



Snapshotting Your Project
=========================

You can incrementally stage what you
want to commit, or you can commit everything at once.

`git status -s` is used to list the status of all of the changed files that the
project is aware of. The `-s` is for short output.

`git add file1 file2...` is used to add files to your *staging* area
before you *commit* them to the project. Even if the file was tracked
before, you still need to add the new files before proceeding if you
want their changes committed. You will need to do `add` after each
change. Or, simply use `git commit -a`.

`git add .` is used to add all changes and all untracked files.

`git rm filename` remove file from project

`git rm $(git ls-files --deleted)` remove all deleted files from project

`git rm --cached file_name` removes a file that you have added but
not committed yet. Keeps file on disk.

`git reset HEAD <file1> <file2>` unstages files (opposite of add).

`git commit` starts *vim* and then allows you to create a commit
message. Only files which have been added are committed. However, if
instead you use `git commit -a` then all previously tracked files are
re-added and then committed. You would still need to use `git add file1`
to add a new file to be tracked, however.

`git commit -a` adds changes on all tracked files, but does not add new files

`git ls-files` shows all files in the index

`git update-index --assume-unchanged <file>` to stop watching the file will 
still remaining in the index.

Branching and Merging
=====================

By default, there is the *master* branch. Generally, this is good to
keep as the stable version.

`git branch` lists all the current branches. The **\*** shows which
current branch you are in.

`git branch branch_name` creates a new branch.

`git checkout branch_name` to change to the branch.

`git checkout commit_hash` will *checkout* a previous commit.

`git checkout -b branch_name` creates a new branch and switches to it
immediately. Makes a "copy" of the current branch.

ALWAYS make a commit before checking out other branches.[^1]

`git push origin <new_branch_name>` creates new remote branch. First make a
local branch and checkout it, and then do this command to create the remote
branch.

`git checkout -b experimental origin/experimental` to create a local branch from an existing remote branch

`git branch -d branch_name` deletes an entire branch.

`git branch -D crazy-idea` delete branch with uncommited changes.

`git merge branch_name` merges "branch_name" into current branch. General
workflow is: create and checkout branch; make changes and commit; checkout
master; merge branch into master

Rebasing
====================

Rebase allows you to pick which commits
you want, and squashes trivial commits down to a single commit. Pass in
branch that you want to start the rebase from.

`git rebase -i master` rebase the commits since the branch first diverged from
master. -i gives interactive window. Change "pick" to one of the characters
specified in the provided instructions, save and exit vi.

Tracking
========

`git show` or `git show HEAD` to see last commit

`git show HEAD~` to see second to last commit

`git show HEAD~4:index.html` to get a file from a particular revision

`git log` shows the commits in chronological order. Newest is at the top.

`git log <filename>` shows the commits affecting the given file

`git log -p` shows complete diffs for each commit

`git log -p <hash>` shows diffs starting from specific commit

`git log --stat` shows the commits with all commit messages.

`git log --oneline` shows commits w/ abbreviated hash (handy for making a quick
digest)

`git log --pretty=full` title + explanation

`git log --graph --oneline` displays graphical representation of commit history

To show the same graph with the names of the committers, you can create an alias with 
`git config alias.lp 'log --format="%Cblue %h %Cgreen %an %Creset %s" --graph'`
and then just use `git lp`

`git instaweb --httpd=webrick` opens up a browser with log info (very cool). Use
`git instaweb --stop` to stop server.

Tagging
======
`git rev-list v1.1.2 | head -n 1` shows the commit that the tag points to

Undoing Changes
===============

`git commit --amend` to edit the last commit

*HEAD* is a pointer to the latest commit.

`git reset --hard HEAD~1` to go back to previous commit (move backwards by one
commit). `--hard` deletes the most recent change and resets the entire
repository.

`git reset --soft HEAD^` to unset the last commit and leave the changes in the
working tree. The '^' means parent and is the same as ~1. Follow this command
with `git reset` to erase the unset commit from the commit history.

Can also do this on individual files:

`git reset -- analysis.php` resets to the last version

`git reset HEAD~2 analysis.php` resets back two commits

Diff
====

`git diff` shows the current unstaged changes. It does not show changes in
untracked files.

`git diff --cached` shows the current staged changes. Its output is what will be
committed.

To compare commits, you can do `git diff (hash1) (hash2)` or alternatively
`git diff @{1} @{2}` (these are the changes from 1 to 2).

`git diff feature-branch master` compares two branches. If no text spits
out, the most recent COMMITS within each branch are the same.

Git workflow
============

Generally, create a new branch for every issue, bug, or feature you’re
trying to build or fix. That way, you can always have deployable code in
the master branch.

`git stash` saves your current changes without commiting them and
reverts back to the previous commit. Works for both staged or unstaged
pages. Matches the HEAD commit.

`git stash pop` then opens this back up.

Remote
======

`git remote show origin` shows the configuration with your remote repository.

Config
======

`git config -l` shows current config settings.

`git config --global color.ui auto` to add color.

If you are getting an error with vi when commiting (on Mac), use
`git config --global core.editor /usr/bin/vim`

`git config --global core.diff opendiff` to change diff program.

The "--global" changes for all repositories, otherwise only for the current one.

Use `git help config` to see all config options.

[^1]: <http://www.gitguys.com/topics/switching-branches-without-committing/>
